﻿
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier

# definire retea de neuroni cu fct relu
# puncte de intrare
X = np.array([0, 0, 0, 1, 1, 0, 1, 1]).reshape(4, 2)
# labels = clase
y = np.array([0, 1, 1, 0]).reshape(4,)

mlp = MLPClassifier(hidden_layer_sizes=(2,), activation='relu')

mlp.fit(X, y)
# Antrenare ponderi manual cu coefs.
# Din desenul pt relu : Pt primul strat: -1,1; 1,-1; Pt al doilea strat: 1,1
mlp.coefs_ = [np.array([[ -1.0 ,  1.0], [1.0, -1.0]]), np.array([[1.0],[1.0]])]

# coef primul strat -0.5, -0.5; Pt al doilea strat : 0
mlp.intercepts_ = [np.array([-0.5, -0.5]), np.array([0.0])]

# prediction
y_pred=mlp.predict(X)   

print("prediction", y_pred)


#afisare suprafata decizie

# generez 1000 pct random in 0,1 (np.random.rand) cu 2 componente (abscisa, ordonata)
points = np.random.rand(1000,2)

# clasificare/prezicere
labels = mlp.predict(points)

plt.ylim((0, 1))    
plt.xlim((0, 1))

plt.plot(points[labels == 0, 0], points[labels == 0, 1], 'ro')
plt.plot(points[labels == 1, 0], points[labels == 1, 1], 'g+')
plt.show()


#invatare retea
# set the method
# hidden_layer_sizes = (10, 10) f bun; (2, 0) nu e bun.
mlp = MLPClassifier(hidden_layer_sizes=(10,), activation='relu',solver='lbfgs',alpha=0.001, max_iter=10000)
mlp.max_iter=10000

# training
mlp.fit(X, y)     
               
# prediction
y_pred=mlp.predict(X)            
print(y_pred)

#afisare suprafata decizie
points = np.random.rand(1000,2)
labels = mlp.predict(points)

plt.ylim((0, 1))
plt.xlim((0, 1))

plt.plot(points[labels == 0, 0], points[labels == 0, 1], 'ro')
plt.plot(points[labels == 1, 0], points[labels == 1, 1], 'g+')
plt.show()


#  Scrieți o funcție care să implementeze funcţia indicator a triunghiului ABC de vârfuri A(-1,0), B(0,1), C(1,0).
#  Funcţia indicator ia valoarea 1 pentru punctele din interiorul triunghiului şi de pe frontieră şi 0 în rest.

# 3 perceptoni pe primul strat pt fiecare dreapta a triunghiului .
# Primul da 0 la stanga, 1 la dreapta. Al doilea da 0 la dreapta, 1 la st. Al treilea 1 sus, 0 jos. Functia este AND intre ele.
# Dreptele:
# AB: x1 - x2 + 1 = 0  -> x1 - x2 + 1 >= 0 primesc 1
# BC: x1 + x2 - 1 = 0  -> x1 + x2 - 1 <= 0 primesc 1
# AC: x2 = 0  -> x2 >= 0 primesc 1

def functia_indicator_ABC(x1, x2):
    if (x1 - x2 + 1 >= 0) and (x1 + x2 - 1 <= 0) and (x2 >= 0):
        return 1
    return 0

# generam 20000 pct in [-2, 2]
points = np.random.rand(20000, 2) * (2 - (-2)) - 2
# etichetez cu 0
labels = np.zeros(20000)

for i in range(20000):
    labels[i] = functia_indicator_ABC(points[i,0], points[i, 1])

plt.ylim((-2,2))
plt.xlim((-2,2))

plt.plot(points[labels == 0, 0], points[labels == 0, 1], 'ro')
plt.plot(points[labels == 1, 0], points[labels == 1, 1], 'g+')
plt.show()

train_points = points[:10000, :]
train_labels = labels[:10000]
test_points = points[10000:20000, :]
test_labels = labels[10000:20000]

model = MLPClassifier(hidden_layer_sizes=(10, ), activation='relu', max_iter=10000, alpha = 0.01)
model.fit(train_points, train_labels)
labels_predicted = model.predict(test_points)


score = model.score(test_points, test_labels)
print(score)


